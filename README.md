# abuild builder for v3.7

This docker image has `abuild` setup to build `.apk` packages.

To get started

```
$ cd abuild-builder/

$ docker build -t abuild-builder .
```

Checkout appropriate git branch such as - `3.7-stable-minnow3`,
`3.7-stable-qemu_x86_64` (from `aports-dom0`), `3.7-stable-minnow3-gbe` (from
`aports-dom1-dd`), `3.7-stable-x86_64-pvcalls` (from `aports-dom1-sd`).

Go to the _parent_ directory containing `aports` tree.

```
$ cd ViryaOS

$ tree -L 1 .
.
|-- abuild-builder
|-- abuild-builder-cross-aarch64
|-- apkrepo
|-- aports-dom0
|-- aports-dom1-dd
|-- aports-dom1-sd
|-- gen-kernel-config-patch
`-- imagebuilder

$ docker run --rm -ti -v $(pwd):/home/builder/src \
     -v <PATH_TO_REPO_BASE_ON_HOST>:/home/builder/apkrepo/<REPO_BASE> \
     abuild-builder /bin/su -l -s /bin/sh builder

(For example)
$ docker run --rm -ti -v $(pwd):/home/builder/src \
     -v /home/virya/work/ViryaOS-apk/apkrepo/qemu_x86_64/v3.7:/home/builder/apkrepo/qemu_x86_64/v3.7 \
     abuild-builder /bin/su -l -s /bin/sh builder

(inside the docker container)
f5c1eee20ebe:~$ sudo apk update

(Assuming the branch checked out is 3.7-stable-qemu_x86_64)
f5c1eee20ebe:~$ cd src/aports-dom0/main/linux/

f5c1eee20ebe:~/src/aports-dom0/main/linux$ abuild \
    -c -r -P /home/builder/apkrepo/qemu_x86_64/v3.7

f5c1eee20ebe:~/src/aports-dom0/main/linux$ abuild \
    -P /home/builder/apkrepo/qemu_x86_64/v3.7 cleanoldpkg
```
